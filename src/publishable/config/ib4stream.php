<?php


return [
    'base_url'   => env('ORBSTREAM_BASE_URL', ''), // TODO: Change "ORBSTREAM_BASE_URL" To "IB4STREAMAM_BASE_URL"
    'wss_url'    => env('ORBSTREAM_WSS_URL', ''), // TODO: Change "ORBSTREAM_WSS_URL" To "IB4STREAMEAM_WSS_URL"
    'app_id'     => env('ORBSTREAM_APP_ID', ''), // TODO: Change "ORBSTREAM_APP_ID" To "IB4STREAMREAM_APP_ID"
    'app_secret' => env('ORBSTREAM_APP_SECRET', ''), // TODO: Change "ORBSTREAM_APP_SECRET" To "IB4STREAM_APP_SECRET"
];
