<?php

namespace Ibrain\Ib4stream\Providers;

use Illuminate\Support\ServiceProvider;

class Ib4steamServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('ib4stream', function () {
            return new \Ibrain\Ib4stream\Client(
                config('ib4stream.base_url'),
                config('ib4stream.app_id'),
                config('ib4stream.app_secret')
            );
        });
    }

    /**
     * Register.
     *
     * @return void
     */
    public function register()
    {
        $this->registerPublishables();
    }

    /**
     * Register Publishables.
     *
     * @return void
     */
    public function registerPublishables()
    {
        $publishablePath = __DIR__.'/../publishable';

        $this->publishes([
            $publishablePath . '/config' => config_path(),
        ], 'ib4stream.php');

        $this->publishes([
            $publishablePath . '/js' => public_path('js'),
        ], 'ib4stream.js');
    }
}
