<?php

namespace Ibrain\Ib4stream\Facades;

use Illuminate\Support\Facades\Facade;

class Ib4streamFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ib4stream';
    }
}
