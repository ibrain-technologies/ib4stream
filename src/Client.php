<?php

namespace Ibrain\Ib4stream;

use GuzzleHttp\Client as GuzzleClient;

use GuzzleHttp\Psr7\Response as GuzzleResponse;

class Client
{
    /**
     * @var string
     */
    protected $appId;

    /**
     * @var string
     */
    protected $appSecret;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $client;

    /**
     * @param   string  $baseUrl
     * @param   string  $appId
     * @param   string  $appSecret
     */
    public function __construct(string $baseUrl, string $appId, string $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->baseUrl = $baseUrl;
        $this->client = new GuzzleClient;
    }

    /**
     * Generates a safe unique id
     *
     * @return string
     */
    public function uniqid(): string
    {
        return $this->request('/helpers/uniqid', 'get');
    }

    /**
     * Publish a message to the specified channel and optionally specify certain users
     *
     * @param   string  $channel
     * @param   string  $body
     * @param   array   $to
     *
     * @return \stdClass
     */
    public function publish(string $channel, $body, array $to = []): \stdClass
    {
        return $this->request(sprintf('/stream/%s/messages', $this->appId), 'post', compact('channel', 'body', 'to'));
    }

    /**
     * Get the channel clients and clients' count
     *
     * @param   string  $channel
     *
     * @return \stdClass
     */
    public function clients(string $channel): \stdClass
    {
        return $this->request(sprintf('/stream/%s/channels/%s/clients', $this->appId, $channel), 'get');
    }

    /** @ignore */
    protected function request(string $endpoint, string $method, array $body = [])
    {
        $payload = [
            'headers' => [
                'Authorization' => $this->appSecret
            ]
        ];

        if (! empty($body)) {
            $payload['json'] = $body;
        }

        $response = null;

        try {
            $response = $this->client->request($method, $this->getFullUrl($endpoint), $payload);
        } catch (\Exception $e) {
            $response = $e->getResponse();
        }

        $body = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() > 201 || ! $body->success) {
            throw new \Exception(
                $body->message
            );
        }

        return $body->data;
    }

    /** @ignore */
    protected function getFullUrl(string $endpoint): string
    {
        return trim(trim($this->baseUrl, "/") . "/" . trim($endpoint, "/"), "/");
    }
}
