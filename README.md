Ib4stream Laravel SDK
=======================
> client SDK for iBrain/Ib4stream based on `ibrain/ib4stream` package.

Usage
======================
- `composer require ibrain/ib4stream`
- register the service provider `Ibrain\Ib4stream\Providers\Ib4steamServiceProvider::class` in `config/app.php` in `providers`.
- register the facade `Ibrain\Ib4stream\Facades\Ib4streamFacade`
- Publish ib4stream.js `php artisan vendor:publish --tag=ib4stream.js`
- Publish ib4stream.php `php artisan vendor:publish --tag=ib4stream.php`

```php
	Ib4stream::publish("mychannel");
```

```html
<script src="js/ib4stream.js" type="text/javascript"></script>
```

```js
window.options = {
	url: "{{ config('ib4stream.wss_url') }}",
	appId: "{{ config('ib4stream.app_id') }}"
}

Ib4stream(options, (os) => {
	os.subscribe("mychannel", (data) => {
	    // Action Here
	});
});
```
